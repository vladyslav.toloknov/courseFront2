import { List, Record } from 'immutable'
import { SIGN_OUT_SUCCESS } from 'src/modules/auth/action-types'
import {
    CREATE_TERRITORY_SUCCESS,
    LOAD_TERRITORY_SUCCESS,
    UPDATE_TERRITORY_SUCCESS
} from './action-types'

export const TerritoriesState = new Record({
    list: new List(),
})

export function territoriesReducer (state = new TerritoriesState(), {payload, type}) {
    switch (type) {
        case CREATE_TERRITORY_SUCCESS:
            return state.merge({
                list: state.list.unshift(payload),
            })

        case LOAD_TERRITORY_SUCCESS:
            return state.set('list', new List(payload.reverse()))

        case UPDATE_TERRITORY_SUCCESS:
            // console.log('payload: ', payload)
            return state.merge({
                list: state.list.map(territory => {
                    // console.log(territory, payload)
                    return territory.id === payload.id ? payload : territory;
                })
            });

        case SIGN_OUT_SUCCESS:
            return new TerritoriesState()

        default:
            return state
    }
}

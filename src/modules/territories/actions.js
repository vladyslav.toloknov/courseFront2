import territoryApi from '../../api/territoriesApi'
import {
    CREATE_TERRITORY_ERROR,
    CREATE_TERRITORY_SUCCESS,
    LOAD_TERRITORY_SUCCESS,
    UNLOAD_TERRITORY_SUCCESS,
    UPDATE_TERRITORY_ERROR,
    UPDATE_TERRITORY_SUCCESS,
} from './action-types'

export function loadTerritorys () {
    return dispatch => {
        territoryApi.getAll()
            .then( territories => dispatch(loadTerritorysSuccess(territories)))
            .catch( error =>{
                throw(error)
            })
    }
}

export function createTerritory (territory) {
    return dispatch => {
        console.log('create', territory)
        territoryApi.addTerritory(territory)
            .then( newTerr => dispatch(createTerritorySuccess(newTerr)) )
            .catch( error =>{
                throw(error)
            })
    }
}

export function updateTerritory (territory, id) {
    console.log('up', territory, id)
    return dispatch => {
        territoryApi.editTerritory(territory, id)
            .then( territory => {
                dispatch(updateTerritorySuccess(territory))
            })
            .catch( error =>{
                throw(error)
            })
    }
}

/////////

export function loadTerritorysSuccess (territories) {
    return {
        type: LOAD_TERRITORY_SUCCESS,
        payload: territories,
    }
}

export function createTerritorySuccess (territory) {
    return {
        type: CREATE_TERRITORY_SUCCESS,
        payload: territory,
    }
}

export function updateTerritorySuccess (territory) {
    return {
        type: UPDATE_TERRITORY_SUCCESS,
        payload: territory,
    }
}
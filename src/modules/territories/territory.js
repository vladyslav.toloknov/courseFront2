import { Record } from 'immutable';


export const Territory = new Record({
  id: null,
  north: null,
  east:null,
  south: null,
  west: null
});

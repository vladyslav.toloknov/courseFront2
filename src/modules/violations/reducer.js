import { List, Record } from 'immutable'
import { SIGN_OUT_SUCCESS } from 'src/modules/auth/action-types'
import {
    LOAD_VIOLATIONS_SUCCESS,
    UNLOAD_VIOLATIONS_SUCCESS
} from './action-types'

export const ViolationsState = new Record({
    list: new List(),
})

export function violationsReducer (state = new ViolationsState(), {payload, type}) {
    switch (type) {
        case LOAD_VIOLATIONS_SUCCESS:
            return state.set('list', new List(payload))

        case SIGN_OUT_SUCCESS:
            return new ViolationsState()

        default:
            return state
    }
}

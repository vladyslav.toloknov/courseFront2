
import {
    LOAD_VIOLATIONS_SUCCESS,
    UNLOAD_VIOLATIONS_SUCCESS
} from './action-types'
import violationApi from '../../api/violationsApi'

export function loadViolationsByDrone (droneId) {
    return (dispatch) => {
        let drones = violationApi.getViolationsByDrone(droneId)
        return dispatch(loadDronesSuccess(drones))
        // return droneApi.getAllDrones().then(cats => {
        //     dispatch(LOAD_TASKS_SUCCESS(cats))
        // }).catch(error => {
        //     throw(error)
        // })
    }
}

export function loadDronesSuccess (violations) {
    return {
        type: LOAD_VIOLATIONS_SUCCESS,
        payload: violations,
    }
}
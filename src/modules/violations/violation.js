import { Record } from 'immutable'

export const Violation = new Record({
    id: null,
    droneId: null,
    territoryId: null,
    info: null
})

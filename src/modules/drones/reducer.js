import { List, Record } from 'immutable'
import { SIGN_OUT_SUCCESS } from 'src/modules/auth/action-types'
import {
    CREATE_DRONE_SUCCESS,
    LOAD_DRONES_SUCCESS,
} from './action-types'

export const DronesState = new Record({
    list: new List(),
})

export function dronesReducer (state = new DronesState(), {payload, type}) {
    switch (type) {
        case CREATE_DRONE_SUCCESS:
            return state.merge({
                list: state.list.unshift(payload),
            })

        case LOAD_DRONES_SUCCESS:
            return state.set('list', new List(payload.reverse()))

        case SIGN_OUT_SUCCESS:
            return new DronesState()

        default:
            return state
    }
}

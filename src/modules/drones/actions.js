import droneApi from '../../api/dronesApi'
import {
    CREATE_DRONE_ERROR,
    CREATE_DRONE_SUCCESS,
    LOAD_DRONES_SUCCESS,
    UNLOAD_DRONES_SUCCESS,
} from './action-types'

export function loadDrones () {
    return (dispatch) => {
        droneApi.getAllDrones()
            .then(drones => dispatch(loadDronesSuccess(drones)))
            .catch(error => {
                throw(error)
            })
    }
}
let i = 10
export function createDrone (descr) {
    return dispatch => {
        droneApi.addDrone(descr)
        return dispatch(createDroneSuccess({id: i++, description: descr}))
    }
}


export function loadDronesSuccess (drones) {
    return {
        type: LOAD_DRONES_SUCCESS,
        payload: drones,
    }
}

export function createDroneSuccess (drone) {
    return {
        type: CREATE_DRONE_SUCCESS,
        payload: drone,
    }
}
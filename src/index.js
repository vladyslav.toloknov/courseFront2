import './views/styles/styles.css'

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'

// import { initAuth } from './auth';
import history from './history'
import configureStore from './store'
import registerServiceWorker from './utils/register-service-worker'
import App from './views/app'
import { loadDrones } from './modules/drones/actions'
import { loadTerritorys } from './modules/territories/actions'

const store = configureStore()
const rootElement = document.getElementById('root')

store.dispatch(loadDrones())
store.dispatch(loadTerritorys())

function render (Component) {
    ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <div>
                    <Component/>
                </div>
            </ConnectedRouter>
        </Provider>,
        rootElement,
    )
}

if (module.hot) {
    module.hot.accept('./views/app', () => {
        render(require('./views/app').default)
    })
}

registerServiceWorker()

render(App)

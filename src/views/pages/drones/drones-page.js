import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import classNames from 'classnames'
import { dronesActions } from 'src/modules/drones'

import { violationsActions } from 'src/modules/violations'

import DroneList from '../../components/drone/drone-list'
import Modal from 'react-modal'
import Icon from '../../components/icon/icon'
import Button from '../../components/button/button'
import Navigator from '../../components/navigator/navigator'
import { DroneForm } from '../../components/drone/drone-form/drone-form'
import ViolationsList from '../../components/violation/violation-list'

const customStyles = {
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.64)'
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'rgb(35, 35, 34)',
        border: ' 0px'
    }
}

export class DronesPage extends Component {
    constructor () {
        super()

        this.state = {
            modalIsOpen: false
        }

        this.openModal = this.openModal.bind(this)
        this.afterOpenModal = this.afterOpenModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }

    openModal () {
        this.setState({modalIsOpen: true})
    }

    afterOpenModal () {
        this.subtitle.style.color = 'rgb(224, 220, 220)'
        this.subtitle.style.fontSize = '2em'
        this.subtitle.style.fontWeight = '300'
    }

    closeModal () {
        this.setState({modalIsOpen: false})
    }

    componentWillMount () {
        if (!this.props.drones)
            this.props.loadDrones()
    }


    render () {
        return (
            <div className="g-row">
                <div className="g-col">
                    <Navigator/>
                    <DroneList
                        openViolations={this.props.loadViolationsByDrone}
                        drones={this.props.drones}
                    />
                </div>
                <div>
                    <Button
                        className={classNames('btn--icon', 'drone-item__button')}
                        onClick={this.openModal}>
                        <Icon name="add"/>
                    </Button>
                    <Modal
                        isOpen={this.state.modalIsOpen}
                        onAfterOpen={this.afterOpenModal}
                        onRequestClose={this.closeModal}
                        style={customStyles}
                    >
                        <a ref={subtitle => this.subtitle = subtitle}>Add new drone</a>
                        <Button
                            className={classNames('btn-close', 'btn--icon', 'drone-item__button')}
                            onClick={this.closeModal}>
                            <Icon name="clear"/>
                        </Button>
                        <DroneForm
                            handleSubmit={this.props.createDrone}
                            close={this.closeModal}
                        >
                        </DroneForm>
                    </Modal>
                </div>
                <ViolationsList>

                </ViolationsList>
            </div>
        )
    }
}

//=====================================
//  CONNECT
//-------------------------------------
//
const mapStateToProps = createSelector(
    // getDroneFilter,
    // getVisibleDrones
)

const mapDispatchToProps = Object.assign(
    {},
    dronesActions,
    violationsActions
)

export default connect(
    state => ({
        drones: state.drones.list
    }),
    mapDispatchToProps,
)(DronesPage)

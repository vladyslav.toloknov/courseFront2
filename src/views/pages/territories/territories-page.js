import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import classNames from 'classnames'
import { territoriesActions } from 'src/modules/territories'

import TerritoryList from '../../components/territory/territory-list'
import Modal from 'react-modal'
import Icon from '../../components/icon/icon'
import Button from '../../components/button/button'
import Navigator from '../../components/navigator/navigator'
import Map from '../../components/map/map'

const customStyles = {
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.64)'
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'rgb(35, 35, 34)',
        border: ' 0px'
    }
}
const coordDiv = {
    width: '49%',
    display: 'inline-block'
}

export class TerritoriesPage extends Component {
    constructor () {
        super()

        this.state = {
            modalIsOpen: false
        }
        this.state.coord = {
            north: 0,
            east: 0,
            south: 0,
            west: 0
        }

        this.openModal = this.openModal.bind(this)
        this.afterOpenModal = this.afterOpenModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.onMapChanged = this.onMapChanged.bind(this)

        this.openEdit = this.openEdit.bind(this)
        this.handle = this.handle.bind(this)
        this.state.editing = { }
    }

    openModal () {
        this.setState({modalIsOpen: true})
    }

    afterOpenModal () {
        this.subtitle.style.color = 'rgb(224, 220, 220)'
        this.subtitle.style.fontSize = '2em'
        this.subtitle.style.fontWeight = '300'
    }

    closeModal () {
        this.setState({modalIsOpen: false})
        this.state.edit = false
    }

    componentWillMount () {
        if(!this.props.territories)
            this.props.loadTerritorys()
    }

    onMapChanged (data) {
        this.setState({
            coord: {
                north: data.f.f,
                east: data.b.b,
                south: data.f.b,
                west: data.b.f
            }
        })
    }
    openEdit(id, bounds){
        this.state.edit = true
        this.state.editing = this.state.editing? this.state.editing: {}
        this.state.editing.bounds = bounds
        this.state.editing.id = id

        this.openModal()
    }

    handle(){
        if(this.state.edit)
            this.props.updateTerritory(this.state.coord, this.state.editing.id)
        else
            this.props.createTerritory(this.state.coord)

        this.closeModal()
    }


    render () {
        return (
            <div className="g-row">
                <div className="g-col">
                    <Navigator/>
                    <TerritoryList
                        openEdit={this.openEdit}
                        territories={this.props.territories}
                    />
                </div>
                <div>
                    <Button
                        className={classNames('btn--icon', 'drone-item__button')}
                        onClick={this.openModal}>
                        <Icon name="add"/>
                    </Button>
                    <Modal
                        ariaHideApp={false}
                        isOpen={this.state.modalIsOpen}
                        onAfterOpen={this.afterOpenModal}
                        onRequestClose={this.closeModal}
                        style={customStyles}>

                        <a ref={subtitle => this.subtitle = subtitle}>Add new territory</a>
                        <Button
                            className={classNames('btn-close', 'btn--icon', 'territory-item__button')}
                            onClick={this.closeModal}>
                            <Icon name="clear"/>
                        </Button>
                        <Map
                            bounds={ this.state.edit
                                ? this.state.editing.bounds
                                : null
                            }
                            handler={this.onMapChanged}>
                        </Map>

                        <div style={coordDiv}>
                            <p>North: {this.state.coord.north}</p>
                            <p>East: {this.state.coord.east}</p>
                        </div>
                        <div style={coordDiv}>
                            <p>South: {this.state.coord.south}</p>
                            <p>West: {this.state.coord.west}</p>
                        </div>
                        <Button
                            className={classNames('btn--icon',
                                'drone-item__button')}
                            type="submit"
                            onClick={this.handle}>
                            <Icon name="done"/>
                        </Button>
                    </Modal>
                </div>
            </div>
        )
    }
}

//=====================================
//  CONNECT
//-------------------------------------
//
const mapStateToProps = createSelector(
    // getTerritoryFilter,
    // getVisibleTerritories
)
const mapDispatchToProps = Object.assign(
    {},
    territoriesActions
)

export default connect(
    state => ({
        territories: state.territories.list
    }),
    mapDispatchToProps,
)(TerritoriesPage)

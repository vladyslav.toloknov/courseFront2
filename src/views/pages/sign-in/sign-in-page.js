import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { authActions } from 'src/modules/auth'
import Button from 'src/views/components/button'

import './sign-in-page.css'

const SignInPage = ({signIn}) => {

    let email = ''
    let password = ''

    const handleChange = () => {
        let emailValue = email.value
        let passwordValue = password.value

        if (emailValue && passwordValue) {
            signIn(emailValue, passwordValue)
        }
    }

    return (
        <div className="g-row sign-in">
            <div className="g-col">
                <h1 className="sign-in__heading">Log in</h1>
                <form className="task-form" onSubmit={handleChange} noValidate>
                    <input
                        autoComplete="false"
                        className="task-form__input"
                        placeholder="What needs to be done?"
                        type="email"
                        ref={input => { email = input }}
                    />
                    <input
                        autoComplete="false"
                        className="task-form__input"
                        placeholder="What needs to be done?"
                        type="password"
                        ref={input => { password = input }}
                    />
                    <Button onClick={handleChange} className="sign-in__button">Log in</Button>
                </form>
            </div>
        </div>
    )
}

//=====================================
//  CONNECT
//-------------------------------------

const mapDispatchToProps = {
    signIn: authActions.signIn,
}

export default withRouter(
    connect(
        null,
        mapDispatchToProps,
    )(SignInPage),
)



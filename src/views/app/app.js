import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { authActions, getAuth } from 'src/modules/auth';
import Header from '../components/header';
import RequireAuthRoute from '../components/require-auth-route';
import RequireUnauthRoute from '../components/require-unauth-route';
import SignInPage from '../pages/sign-in';
import DronesPage from '../pages/drones'
import TerritoriesPage from '../pages/territories'

const App = ({authenticated, user, signOut}) => (
  <div>
    <Header
      authenticated={authenticated}
      signOut={signOut}
    />
    <main>
      <RequireAuthRoute authenticated={authenticated} user={user} exact path="/drones" component={DronesPage}/>
      <RequireAuthRoute authenticated={authenticated} user={user} exact path="/territories" component={TerritoriesPage}/>
      <RequireUnauthRoute authenticated={authenticated} path="/" component={SignInPage}/>
    </main>
  </div>
);

App.propTypes = {
  authenticated: PropTypes.bool.isRequired,
  signOut: PropTypes.func.isRequired
};


//=====================================
//  CONNECT
//-------------------------------------

const mapStateToProps = getAuth;

const mapDispatchToProps = {
  signOut: authActions.signOut
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);

import React from 'react'

class Map extends React.Component {

    constructor () {
        super()

        this.initMap = this.initMap.bind(this)
        this.handle = this.handle.bind(this)
    }

    componentDidMount () {
        window.initMap = this.initMap
        loadJS(
            'https://maps.googleapis.com/maps/api/js?key=AIzaSyBPaG_Yt2ZVGauO7fxSMKz8YjUlxZjVCK8&callback=initMap')
    }

    handle (data) {
        this.props.handler(data)
    }

    initMap () {
        const google = window.google

        var bounds = this.props.bounds || {
            north: 44.599,
            south: 44.490,
            east: -78.443,
            west: -78.649,
        }

        this.map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: (bounds.north + bounds.south) / 2,
                lng: (bounds.east + bounds.west) / 2
            },
            zoom: 9,
        })

        var rectangle = new google.maps.Rectangle({
            bounds: bounds,
            editable: true,
            draggable: true,
            geodesic: true,
        })

        // pice of shit
        function handle (rectangle) {
            this.handle(rectangle.bounds)
        }

        handle = handle.bind(this)
        google.maps.event.addListener(rectangle, 'bounds_changed', function () {
            // console.log('nc.', rectangle.bounds.b)
            // console.log('rc.', rectangle.bounds.f)
            handle(rectangle)
        })
        rectangle.setMap(this.map)
    }

    st = {
        margin: 20,
        height: 300,
        width: 500,
        'overflow': 'auto'
    }

    render () {
        return (
            <div id="map" ref="this.map" style={this.st}>
            </div>
        )
    }
}

function loadJS (src) {
    var ref = window.document.getElementsByTagName('script')[0]
    var script = window.document.createElement('script')
    script.src = src
    script.async = true
    ref.parentNode.insertBefore(script, ref)
}

export default Map

import React from 'react';
import { NavLink } from 'react-router-dom';

import './navigator.css';

const Navigator = () => (
  <ul className="task-filters" >
    <li><NavLink to="/drones">Drones</NavLink></li>
    <li><NavLink to="/territories">Territories</NavLink></li>
  </ul>
);


export default Navigator;

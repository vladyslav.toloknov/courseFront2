import React from 'react'
import './territory-item.css'
import Button from '../../button/button'
import Icon from '../../icon/icon'
import classNames from 'classnames'

const TerritoryItem = ({openEdit, key, territory}) =>
    <div className='drone-item' tabIndex="0">
        <div className="cell">
            <div className="drone-item__title" tabIndex="0">
                {territory.north + ' | ' +
                territory.east + ' | ' +
                territory.south + ' | ' +
                territory.west}
            </div>

        </div>
        <Button
            className={classNames('btn--icon', 'drone-item__button')}
            onClick={openEdit.bind(this, territory.id, {
                north: territory.north,
                east: territory.east,
                south: territory.south,
                west: territory.west,
            })}>
            <Icon name="edit"/>
        </Button>
    </div>

export default TerritoryItem

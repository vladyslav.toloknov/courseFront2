import React from 'react'
import TerritoryItem from '../territory-item/territory-item'

const TerritoryList = ({openEdit, territories}) => {
    let territoryItems = territories.map((territory, index) =>
        <TerritoryItem
            openEdit={openEdit}
            key={territory.id}
            territory={territory}
        />,
    )
    return (
        <div className="drone-list">
            {territoryItems}
        </div>
    )
}

export default TerritoryList

import React from 'react';
import { Route, Redirect } from 'react-router-dom'


const RequireUnauthRoute = ({component: Component, authenticated, user,...rest}) => (
  <Route
    {...rest}
    render={props => {
      return authenticated ? (
        <Redirect to={{
          pathname: (user && user.role === 'admin')
            ? '/limitations'
            : '/drones',
          state: {from: props.location}
        }}/>
      ) : (
        <Component {...props}/>
      )
    }}
  />
);


export default RequireUnauthRoute;

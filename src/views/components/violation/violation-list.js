import React from 'react'
import ViolationsItem from './violation-item'
import { connect } from 'react-redux'

import { violationsActions } from 'src/modules/violations'

class ViolationsList extends React.Component{

    componentWillMount () {
        if(!this.props.violations)
            this.props.loadViolationsByDrone()
    }

    render() {
        return (
            <div className="drone-list">
                {this.props.violations.map((violation, index) =>
                    <ViolationsItem
                        key={violation.id}
                        info={violation.info}
                    />
                )}
            </div>
        )
    }
}

const mapDispatchToProps = Object.assign(
    {},
    violationsActions
)

export default connect(
    state => ( {
        violations: state.violations.list
    }),
    mapDispatchToProps,
)(ViolationsList)


import React from 'react'

const ViolationsItem = ({key, info}) =>
    <div className='drone-item' tabIndex="0">
        <div className="cell">
            <div className="drone-item__title" tabIndex="0">
                {info}
            </div>

        </div>
    </div>

export default ViolationsItem

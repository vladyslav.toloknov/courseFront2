import React, { Component } from 'react'

import './drone-form.css'
import Button from '../../button/button'
import Icon from '../../icon/icon'

import classNames from 'classnames';

export class DroneForm extends Component {
    // static propTypes = {
    //   handleSubmit: PropTypes.func.isRequired
    // };

    constructor () {
        super(...arguments)

        this.state = {title: ''}

        this.handleChange = this.handleChange.bind(this)
        this.handleKeyUp = this.handleKeyUp.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    clearInput () {
        this.setState({title: ''})
    }

    handleChange (event) {
        this.setState({title: event.target.value})
    }

    handleKeyUp (event) {
        if (event.keyCode === 27) this.clearInput()
    }

    handleSubmit (event) {
        event.preventDefault()
        const title = this.state.title.trim()
        if (title.length) this.props.handleSubmit(title)
        this.clearInput()
        this.props.close()
    }

    render () {
        return (
            <form className="drone-form" onSubmit={this.handleSubmit}
                  noValidate>
                <input
                    autoComplete="off"
                    autoFocus
                    className="drone-form__input"
                    maxLength="64"
                    onChange={this.handleChange}
                    onKeyUp={this.handleKeyUp}
                    placeholder="What needs to be done?"
                    ref={e => this.titleInput = e}
                    type="text"
                    value={this.state.title}
                />
                <Button
                    className={classNames('btn--icon',
                        'drone-item__button')}
                    type="submit">
                    <Icon name="done"/>
                </Button>
            </form>
        )
    }
}

export default DroneForm

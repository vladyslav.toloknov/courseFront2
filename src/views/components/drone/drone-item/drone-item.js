import React from 'react'
import './drone-item.css'
import Button from '../../button/button'
import Icon from '../../icon/icon'

import classNames from 'classnames'

const DroneItem = ({openViolations,drone}) =>
    <div className='drone-item' tabIndex="0">
        <div className="cell">
            <div className="drone-item__title" tabIndex="0">
                {drone.description}
            </div>
        </div>
        <Button
            className={classNames('btn--icon', 'drone-item__button')}
            onClick={openViolations.bind(this, drone.id)}>
            <Icon name="info"/>
        </Button>
    </div>

export default DroneItem

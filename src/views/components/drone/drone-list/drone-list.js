import React from 'react'
import DroneItem from '../drone-item/drone-item'

const DroneList = ({openViolations, drones}) => {
    let droneItems = drones.map((drone, index) =>
        <DroneItem
            openViolations={openViolations}
            key={index}
            drone={drone}
        />,
    )
    return (
        <div className="drone-list">
            {droneItems}
        </div>
    )
}
//
// DroneList.propTypes = {
//   drones: PropTypes.instanceOf(List).isRequired,
// };

export default DroneList

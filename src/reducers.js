import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import { authReducer } from './modules/auth';
import { dronesReducer } from './modules/drones/reducer'
import { territoriesReducer } from './modules/territories/reducer'
import { violationsReducer } from './modules/violations/reducer'


export default combineReducers({
  auth: authReducer,
  routing: routerReducer,
  drones: dronesReducer,
  territories: territoriesReducer,
  violations: violationsReducer,
});

const violations = [
    {
        id: 1,
        droneId: 1,
        territoryId: 2,
        info: 'fucking drones'
    },
    {
        id: 2,
        droneId: 1,
        territoryId: 1,
        info: 'fuck fucking drones'
    },
    {
        id: 3,
        droneId: 1,
        territoryId: 3,
        info: 'fucking fuck'
    },
    {
        id: 41,
        droneId: 2,
        territoryId: 1,
        info: '..drones'
    },
    {
        id: 5,
        droneId: 1,
        territoryId: 2,
        info: 'also fucking drones'
    },
]

const token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MjU4NzUwNzMsImlkIjoxfQ.XNrMxMHlVWvyltNiaM17IFqXqyEjL6OWCl055tgnUe0'

class ViolationApi {
    static getViolationsByDrone (dId) {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token,
            },
        }

        return fetch(
            'http://app-25040600-f7b9-4955-bd7d-8a1d6fc3940b.cleverapps.io/violations/' + dId,
            requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText)
            }
            return response.json()
        })
        .then(data =>  data.map(d => ({
                id      : d.id,
                north   : d.north,
                east    : d.east,
                south   : d.south,
                west    : d.west,
            }))
        )
    }
}

export default ViolationApi
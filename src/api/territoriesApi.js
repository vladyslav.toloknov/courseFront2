let territories = []

const token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MjU4NzUwNzMsImlkIjoxfQ.XNrMxMHlVWvyltNiaM17IFqXqyEjL6OWCl055tgnUe0'

class TerritoryApi {
    static getAll() {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token,
            },
        }

        return fetch(
            'http://app-25040600-f7b9-4955-bd7d-8a1d6fc3940b.cleverapps.io/territory',
            requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText)
            }
            return response.json()
        })
        .then(data =>  data.map(d => ({
                    id      : d.id,
                    north   : d.north,
                    east    : d.east,
                    south   : d.south,
                    west    : d.west,
                }))
        )
    }

    static addTerritory (coords) {
        console.log('add info', JSON.stringify(coords))

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify(coords),
        }
        return fetch(
            'http://app-25040600-f7b9-4955-bd7d-8a1d6fc3940b.cleverapps.io/territory',
            requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText)
            }
            return response.json()
        })
        .then(territory => {
            console.log('terr', territory)
            territory = {
                id      : territory.id,
                north   : territory.north,
                east    : territory.east,
                south   : territory.south,
                west    : territory.west,

            }

            return territory
        })
    }

    //edit
    static editTerritory (terr, id) {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify(terr),
        }
        return fetch(
            'http://app-25040600-f7b9-4955-bd7d-8a1d6fc3940b.cleverapps.io/territory/' + id,
            requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText)
            }
            return response.json()
        })
        .then(territory => {
            territory = {
                id      : territory.id,
                north   : territory.north,
                east    : territory.east,
                south   : territory.south,
                west    : territory.west,

            }
            return territory
        })
    }
}

export default TerritoryApi
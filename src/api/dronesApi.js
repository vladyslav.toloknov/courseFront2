let drones = []
const token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MjU4NzUwNzMsImlkIjoxfQ.XNrMxMHlVWvyltNiaM17IFqXqyEjL6OWCl055tgnUe0'
class DroneApi {

    static getAllDrones () {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token,
            },
        }

        return fetch(
            'http://app-25040600-f7b9-4955-bd7d-8a1d6fc3940b.cleverapps.io/drones',
            requestOptions)
            .then(response => {
                if (!response.ok) {
                    return Promise.reject(response.statusText)
                }
                return response.json()
            })
            .then(data => {
                console.log(data)
                    drones = data.map(e => ({
                        id: e.id,
                        description: e.description,
                    }))
                    return drones
                },
            )
    }

    static addDrone (description) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({ description }),
        }
        return fetch(
            'http://app-25040600-f7b9-4955-bd7d-8a1d6fc3940b.cleverapps.io/drone',
            requestOptions)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.statusText)
            }
            return response.json()
        })
        .then(drone => {
            drone = {
                id: drone.id,
                description: drone.description,
            }
            drones.push(drone)
            return true
        })
    }
}

export default DroneApi